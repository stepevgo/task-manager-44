package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDTOService getSessionService();

}
